import 'dart:io';

int faktorial(int n) {
  if (n <= 0) {
    return 1;
  }

  return n * faktorial(n - 1);
}

void main() {
    stdout.write("masukan angka : ");
    int angka = int.parse(stdin.readLineSync()!);
    int hasilfaktorial = faktorial(angka);
    print("$angka! = $hasilfaktorial");
}