import 'dart:io';
void main() {
  stdout.write('Mau menginstall aplikasi? (Y/T): ');
  String jawaban = stdin.readLineSync() ?? '';

  String pesan = (jawaban.toLowerCase() == 'y') ? 'Anda akan menginstall aplikasi Dart' : 'Aborted';

  print(pesan);
}