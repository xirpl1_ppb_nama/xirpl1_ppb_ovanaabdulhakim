// Import library untuk input dari pengguna
import 'dart:io';

// Fungsi utama
void main() {
  // Memanggil fungsi untuk mengeksekusi aplikasi
  runApp();
}

// Fungsi eksekusi aplikasi
void runApp() {
  print("=== APLIKASI SEDERHANA ===");

  // Memanggil fungsi untuk menggunakan operator aritmatika
  arithmeticOperators();

  // Memanggil fungsi untuk menggunakan kondisional
  conditionalStatement();

  // Memanggil fungsi untuk menggunakan pengulangan
  loopExample();

  // Memanggil fungsi untuk menggunakan fungsi
  functionExample();
}

// Fungsi untuk menggunakan operator aritmatika
void arithmeticOperators() {
  print("\n-- Operator Aritmatika --");

  // Input dari pengguna
  stdout.write("Masukkan angka pertama: ");
  double ovana1 = double.parse(stdin.readLineSync()!);

  stdout.write("Masukkan angka kedua: ");
  double ovana2 = double.parse(stdin.readLineSync()!);

  // Menampilkan hasil operasi aritmatika
  print("$ovana1 + $ovana2 = ${ovana1 + ovana2}");
  print("$ovana1 - $ovana2 = ${ovana1 - ovana2}");
  print("$ovana1 * $ovana2 = ${ovana1 * ovana2}");
  print("$ovana1 / $ovana2 = ${ovana1 / ovana2}");
}

// Fungsi untuk menggunakan kondisional
void conditionalStatement() {
  print("\n-- Kondisional --");

  // Input dari pengguna
  stdout.write("Masukkan angka: ");
  int angka_ovana = int.parse(stdin.readLineSync()!);

  // Menggunakan pernyataan kondisional
  if (angka_ovana % 2 == 0) {
    print("$angka_ovana adalah bilangan genap.");
  } else {
    print("$angka_ovana adalah bilangan ganjil.");
  }
}

// Fungsi untuk menggunakan pengulangan
void loopExample() {
  print("\n-- Pengulangan --");

  // Pengulangan for
  print("Pengulangan for 1 hingga 5:");
  for (int i = 1; i <= 5; i++) {
    print(i);
  }

  // Pengulangan while
  print("Pengulangan while 1 hingga 5:");
  int j = 1;
  while (j <= 5) {
    print(j);
    j++;
  }
}

// Fungsi untuk menggunakan fungsi
void functionExample() {
  print("\n-- Fungsi --");

  // Memanggil fungsi
  double hasilPenjumlahan = tambahDuaAngka(3.5, 2.5);
  print("Hasil penjumlahan: $hasilPenjumlahan");
}

// Fungsi untuk menambah dua angka
double tambahDuaAngka(double a, double b) {
  return a + b;
}